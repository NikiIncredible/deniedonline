package de.DasNiklas.DeniedOnline.Main;

import de.DasNiklas.DeniedOnline.Listener.PlayerLoginListener;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;

public class Main extends JavaPlugin {

    public static String prefix =  "§cServer §8◆ §7";
    public static String NoPerm = prefix + "§cDu hast darauf keine Rechte!";

    public static File file = new File("plugins//DeniedOnline//config.yml");
    public static YamlConfiguration config = YamlConfiguration.loadConfiguration(file);

    public static Boolean isOnline = false;

    public static Main main;


    @Override
    public void onEnable() {

        Bukkit.getConsoleSender().sendMessage(prefix + "§aPlugin wurde aktiviert");
        main = this;

        getServer().getPluginManager().registerEvents(new PlayerLoginListener(), this);

        config.options().configuration().addDefault("UUID", "f6c3b6fa-2556-4965-9d68-b189e9db2002");
        config.options().configuration().addDefault("delay", 300);
        config.options().copyDefaults(true);
        try {
            config.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (Player p : Bukkit.getOnlinePlayers()) {
            if(Main.config.getString("UUID").equals(p.getUniqueId().toString())) {
                Main.isOnline = true;
                if(PlayerLoginListener.task != null) {
                    PlayerLoginListener.task.cancel();
                }
            } else {
                return;
            }
        }
    }

    @Override
    public void onDisable() {

        Bukkit.getConsoleSender().sendMessage(prefix + "§cPlugin wurde deaktiviert");

    }

}
