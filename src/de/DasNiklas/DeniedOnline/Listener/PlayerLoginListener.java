package de.DasNiklas.DeniedOnline.Listener;

import de.DasNiklas.DeniedOnline.Main.Main;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitTask;

public class PlayerLoginListener implements Listener {

    public static BukkitTask task = null;

    @EventHandler
    public void onLogin(PlayerLoginEvent e) {
        Player p = e.getPlayer();

        if(Main.isOnline == false) {
            if(Main.config.getString("UUID").equals(p.getUniqueId().toString())) {
                Main.isOnline = true;
                if(task != null) {
                    task.cancel();
                }
                e.allow();
            } else {
                if(p.hasPermission("OnlineDenied.allow")) {
                    e.allow();
                } else {
                    e.disallow(PlayerLoginEvent.Result.KICK_FULL, "Du kannst im Moment den Server nicht betreten.");
                }
            }
        } else {
            if(Bukkit.getWhitelistedPlayers().contains(p)) {
                e.allow();
            } else {
                e.disallow(PlayerLoginEvent.Result.KICK_FULL, "Du bist nicht auf der Whitelist.");
            }
        }

    }

    int i = Main.config.getInt("delay");


    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        Player p = e.getPlayer();

        Bukkit.broadcastMessage(Main.prefix + "Der Besitzer ist nicht mehr Online. Der Zugang wird in §c5 Minuten §7gesperrt.");
        if(Main.config.getString("UUID").equals(p.getUniqueId().toString())) {
            Main.isOnline = false;

            task = Bukkit.getScheduler().runTaskLater(Main.main, new Runnable() {
                @Override
                public void run() {

                    for (Player players : Bukkit.getOnlinePlayers()) {
                        players.kickPlayer("Du darfst jetzt nicht mehr weiterspielen. Bitte warte bis der Besitzer wieder Online kommt.");
                    }

                }
            }, Main.config.getInt("delay")*20);

        }

    }

}
